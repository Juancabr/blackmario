package elementos;

import com.blackmario.Mundo;

/**
 * Clase que representa una flor que mata al personaje
 * @author Juan Carlos Beltran
 */
public class Flor extends Enemigo {

    /**
     * Crea la flor
     * @param w Mundo en el que estara la flor
     * @param posX posicion x de la flor
     * @param posY posicion y de la flor
     */
    public Flor(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/items/Flor1.png");
    }
}
