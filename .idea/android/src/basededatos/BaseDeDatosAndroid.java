package basededatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.badlogic.gdx.Gdx;

import java.sql.Timestamp;

public class BaseDeDatosAndroid extends BaseDeDatos{
    private BaseDeDatosAndroidOpenHelper openHelper;
    private SQLiteDatabase baseDatos;

    public BaseDeDatosAndroid(Context c){
        super();
        openHelper=new BaseDeDatosAndroidOpenHelper(c);
        baseDatos=openHelper.getWritableDatabase();
    }

    @Override
    public Puntuaciones cargarPartida() {
        //Hago la consulta a ver si hay una partida en curso (Con fecha de fin igual a null)
        Cursor c=baseDatos.query(BaseDeDatos.getScoreTablename(),null,
                BaseDeDatos.getEnddateField()+" is null",null,
                null,null,null);
        //Si existe una partida guardada ya
        if(c.getCount()>0){
            //Cargo la partida en un objeto Puntuaciones y lo devuelvo
            c.moveToFirst();
            Timestamp fInicio=new Timestamp(c.getLong(c.getColumnIndex(BaseDeDatos.getStartdateField())));
            Timestamp fFin=new Timestamp(c.getLong(c.getColumnIndex(BaseDeDatos.getEnddateField())));
            Puntuaciones ret=new Puntuaciones(fInicio,fFin);
            return ret;
        }else{ //Si no se ha encontrado una partida empezada en la base de datos
            //Creo una nueva partida que comienza ahora
            return new Puntuaciones(new Timestamp(System.currentTimeMillis()),null);
        }
    }

    @Override
    public void guardarPartida(Puntuaciones partida) {
        //Primero compruebo si hay una partida abierta para insertarla o actualizar la que hay
        Cursor c=baseDatos.query(BaseDeDatos.getScoreTablename(),null,
                BaseDeDatos.getEnddateField()+" is null",null,
                null,null,null);

        //Si la partida con fecha final = null existe ya, la modifico
        if(c.getCount()>0) { //Modificar la partida ya abierta
            c.moveToFirst();
            ContentValues valores=new ContentValues();
            baseDatos.update(BaseDeDatos.getScoreTablename(),valores,BaseDeDatos.getEnddateField()+" is null",null);

            //Si la partida con fecha final = null aún no existe, la inserto
        }else{ //insertar una nueva partida
            Gdx.app.log("Voy a insertar","!!!!!!!" );
            ContentValues valores=new ContentValues();
            valores.put(BaseDeDatos.getStartdateField(),partida.getInicioPartida().getTime());
            valores.put(BaseDeDatos.getEnddateField(),"null");
            Long id=baseDatos.insert(BaseDeDatos.getScoreTablename(),null,valores);
            Gdx.app.log("ID insertada",id+"" );
        }
    }

    @Override
    public void finalizarPartida() {

    }

    @Override
    public Puntuaciones[] top3() {
        return new Puntuaciones[0];
    }

}
