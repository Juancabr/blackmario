package actores;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.blackmario.Mundo;

import elementos.Enemigo;

/** Clase que representa el personaje que controla el jugador
 * @author Juan Carlos Beltran
 */
public class Mario extends Actor {
    private Sprite sprite; // Sprite del personaje
    private Mundo mundo; // El mundo en el que va a estar el personaje
    private Body cuerpo; // El cuerpo del personaje que interactuara con el entorno
    private FixtureDef fixtureDef; // La figura que representara al personaje
    private char movimiento; // Caracter que define hacia donde se mueve el personaje

    /**
     * Crea los atributos del personaje para que tengan fisicas con un sprite
     * @param w El mundo en el que va a estar el personaje
     */
    public Mario(Mundo w){

        this.mundo=w;
        //Asignamos la textura
        sprite=new Sprite(new Texture(Gdx.files.internal("personajes/blackMario.png")));
        sprite.setSize(2, 4);

        //Creando cuerpo
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        fixtureDef = new FixtureDef();
        fixtureDef.friction = 1;

        PolygonShape shape = new PolygonShape();

        Vector2[] vertices=new Vector2[4];
        vertices[0]=new Vector2(0,0);
        vertices[1]=new Vector2(sprite.getWidth(),0);
        vertices[2]=new Vector2(sprite.getWidth(),sprite.getHeight());
        vertices[3]=new Vector2(0,sprite.getHeight());
        shape.set(vertices);

        fixtureDef.shape = shape;

        cuerpo = mundo.getWorld().createBody(bodyDef);
        cuerpo.createFixture(fixtureDef);

        cuerpo.setTransform(0, 1, 0);

        shape.dispose();

        EdgeShape edgeShape = new EdgeShape();
        edgeShape.set(-(Gdx.graphics.getWidth()/100f)/2,-(Gdx.graphics.getHeight()/100f)/2,(Gdx.graphics.getWidth()/100f)/2,-(Gdx.graphics.getHeight()/100f)/2);
        fixtureDef.shape = edgeShape;
    }

    /**
     * Funcion que devuelve el cuerpo del personaje
     * @return Body el cuerpo del personaje
     */
    public Body getCuerpo() {
        return cuerpo;
    }

    /**
     * Devuelve la direccion del moviemento del personaje
     * @return char direccion del movimiento del personaje
     */
    public char getMovimiento() {
        return movimiento;
    }

    /**
     * Cambia el movimiento del personaje
     * @param movimiento direccion del movimiento del personaje
     */
    public void setMovimiento(char movimiento) {
        this.movimiento = movimiento;
    }

    /**
     * Dibuja el personaje en el mundo
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        this.setPosition(cuerpo.getPosition().x,cuerpo.getPosition().y);
        this.setRotation(cuerpo.getAngle());

        sprite.setScale(getScaleX(),getScaleY());
        sprite.setRotation(getRotation());
        sprite.setPosition(getX(),getY());
        sprite.setColor(getColor().r,getColor().g,getColor().b,getColor().a);
        sprite.draw(batch);
    }

    /**
     * Identifica si el personaje colisiona con un enemigo
     * @param p Enemigo con el que colisiona el personaje
     * @return si el personaje colisiona con un enemigo
     */
    public boolean colisionaCon(Enemigo p){
        return Intersector.overlaps(this.getHitbox(),p.getHitbox());
    }

    /**
     * Devuelve la hitbox del personaje
     * @return la hitbox del personaje
     */
    public Rectangle getHitbox(){
        return this.sprite.getBoundingRectangle();
    }
}
