package actores;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.blackmario.Mundo;

import elementos.Asesino;

public class Mario extends Actor {
    private Sprite sprite; //Necesitamos una textura para dibujar el Sprite
    private Mundo mundo; //El mundo
    private Body cuerpo;
    private FixtureDef fixtureDef;
    private char movimiento;

    public Mario(Mundo w){

        this.mundo=w;
        //Asignamos la textura
        sprite=new Sprite(new Texture(Gdx.files.internal("personajes/blackMario.png")));
        sprite.setSize(2, 4);

        //Creando cuerpo
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        fixtureDef = new FixtureDef();
        fixtureDef.friction = 1;

        PolygonShape shape = new PolygonShape();

        Vector2[] vertices=new Vector2[4];
        vertices[0]=new Vector2(0,0);
        vertices[1]=new Vector2(sprite.getWidth(),0);
        vertices[2]=new Vector2(sprite.getWidth(),sprite.getHeight());
        vertices[3]=new Vector2(0,sprite.getHeight());
        shape.set(vertices);

        fixtureDef.shape = shape;

        cuerpo = mundo.getWorld().createBody(bodyDef);
        cuerpo.createFixture(fixtureDef);

        cuerpo.setTransform(0, 1, 0);

        shape.dispose();

        EdgeShape edgeShape = new EdgeShape();
        edgeShape.set(-(Gdx.graphics.getWidth()/100f)/2,-(Gdx.graphics.getHeight()/100f)/2,(Gdx.graphics.getWidth()/100f)/2,-(Gdx.graphics.getHeight()/100f)/2);
        fixtureDef.shape = edgeShape;
    }

    public Body getCuerpo() {
        return cuerpo;
    }

    public char getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(char movimiento) {
        this.movimiento = movimiento;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //Si llega a la meta
        if(cuerpo.getPosition().x > 92){
            this.mundo.reconstruir();

            cuerpo.setLinearVelocity(new Vector2(0,0));
            cuerpo.setAngularVelocity(0);
            cuerpo.setTransform(0,2,0);
        }

        super.draw(batch, parentAlpha);
        this.setPosition(cuerpo.getPosition().x,cuerpo.getPosition().y);
        this.setRotation(cuerpo.getAngle());

        sprite.setScale(getScaleX(),getScaleY());
        sprite.setRotation(getRotation());
        sprite.setPosition(getX(),getY());
        sprite.setColor(getColor().r,getColor().g,getColor().b,getColor().a);
        sprite.draw(batch);
    }

    public boolean colisionaCon(Asesino p){
        return Intersector.overlaps(this.getHitbox(),p.getHitbox());
    }

    public Rectangle getHitbox(){
        return this.sprite.getBoundingRectangle();
    }
}
