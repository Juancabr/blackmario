package com.blackmario;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;

import actores.Mario;
import elementos.Nube;
import inputs.TecladoMario;

public class MainBlackMario extends ApplicationAdapter {
    private Mario personaje;
	private Mundo mundo;
    private TecladoMario teclado;
	
	@Override
	public void create () {
        mundo = new Mundo();
        personaje = new Mario(mundo);
        teclado = new TecladoMario(personaje);
        Gdx.input.setInputProcessor(teclado);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mundo.dibujar(this.personaje);

        //movimientos de los enemigos
        mundo.getElementosPeligrosos().get(2).act(Gdx.graphics.getDeltaTime());

        if(personaje.getX() > mundo.getElementosPeligrosos().get(3).getX()-2.5f) {
            mundo.getElementosPeligrosos().get(3).act(Gdx.graphics.getDeltaTime());
        }

        if(personaje.getX() > 16) {
            Nube nube = (Nube) mundo.getElementosPeligrosos().get(4);
            nube.act(Gdx.graphics.getDeltaTime(), personaje);
        }

        gestionColisiones();
	}

    @Override
    public void resize(int width, int height) {
        mundo.redimensionar();
    }
	
	@Override
	public void dispose () {
        mundo.dispose();
	}

    public void gestionColisiones(){
        for (int i = 0; i < mundo.getElementosPeligrosos().size(); i++) {
            if(personaje.colisionaCon(mundo.getElementosPeligrosos().get(i))) {
                this.mundo.reconstruir();

                personaje.getCuerpo().setLinearVelocity(new Vector2(0,0));
                personaje.getCuerpo().setAngularVelocity(0);
                personaje.getCuerpo().setTransform(0,4,0);
            }
        }

    }
}
