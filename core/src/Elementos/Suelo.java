package elementos;

import com.blackmario.Mundo;

/**
 * Clase que representa el suelo por el que se mueven los actores del mundo
 * @author Juan Carlos Beltran
 */
public class Suelo extends Bloque{

    /**
     * Crea el suelo
     * @param w Mundo en el que estara el suelo
     * @param posX posicion x del suelo
     * @param posY posicion y del suelo
     * @param sprite String ruta de la imagen del suelo
     */
    public Suelo(Mundo w, int posX, int posY, String sprite) {
        super(w, posX, posY, sprite);
    }
}
