package basededatos;

import java.sql.Timestamp;

public class Puntuaciones {
    private int muertes;
    private Timestamp inicioPartida;
    private Timestamp finPartida;

    public Puntuaciones(int muertes, Timestamp inicioPartida, Timestamp finPartida) {
        this.muertes = muertes;
        this.inicioPartida = inicioPartida;
        this.finPartida = finPartida;
    }

    public int getMuertes() {
        return muertes;
    }

    public void setMuertes(int muertes) {
        this.muertes = muertes;
    }

    public Timestamp getInicioPartida() {
        return inicioPartida;
    }

    public void setInicioPartida(Timestamp inicioPartida) {
        this.inicioPartida = inicioPartida;
    }

    public Timestamp getFinPartida() {
        return finPartida;
    }

    public void setFinPartida(Timestamp finPartida) {
        this.finPartida = finPartida;
    }


}
