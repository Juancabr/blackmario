package elementos;

import com.blackmario.Mundo;

/**
 * Clase que representa una moneda que puede matar al personaje
 * @author Juan Carlos Beltran
 */
public class Moneda extends Enemigo {

    /**
     * Crea la moneda
     * @param w Mundo en el que estara la moneda
     * @param posX posicion x de la moneda
     * @param posY posicion y de la moneda
     */
    public Moneda(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/items/Moneda1.png");
    }
}
