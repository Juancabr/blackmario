package elementos;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.blackmario.Mundo;

public abstract class Asesino extends Actor {
    private Mundo mundo;
    private Sprite sprite;

    public Asesino(Mundo w, int posX, float posY, float width, float height, String spritePath){
        this.mundo=w;
        //Asignamos la textura
        sprite=new Sprite(new Texture(Gdx.files.internal(spritePath)));
        sprite.setSize(width, height);
        this.setX(posX);
        this.setY(posY);
        sprite.setOrigin((getWidth())/2,(getHeight())/2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        sprite.setScale(getScaleX(),getScaleY());
        sprite.setRotation(getRotation());
        sprite.setPosition(getX(),getY());
        sprite.draw(batch);
    }

    public Rectangle getHitbox(){
        return this.sprite.getBoundingRectangle();
    }
}
