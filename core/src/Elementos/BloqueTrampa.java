package elementos;

import com.blackmario.Mundo;

/**
 * Clase que representa un bloque que se mueve cuando detecta que el personaje esta debajo suya
 * @author Juan Carlos Beltran
 */
public class BloqueTrampa extends Enemigo {

    /**
     * Crea el blque trampa
     * @param w Mundo en el que estara el bloque
     * @param posX posicion x del bloque
     * @param posY posicion y del bloque
     */
    public BloqueTrampa(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/bloques/Bloque3.png");
    }

    /**
     * Funcion que hace que se mueva el bloque hacia abajo cuando detecta que el personaje esta debajo suya
     * @param delta Numero que es mas o menos grande dependiendo de los frames del juego
     */
    @Override
    public void act(float delta) {
        if(getY() > 2.5f){
            setY(getY() - 30 * delta);
        }
    }
}