package com.blackmario;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import java.sql.Timestamp;
import java.util.ArrayList;

import actores.Mario;
import basededatos.Puntuaciones;
import elementos.Enemigo;
import elementos.BloqueTrampa;
import elementos.Flor;
import elementos.Goomba;
import elementos.Moneda;
import elementos.Nube;
import elementos.Suelo;

/**
 * Clase que representa el mundo en el que estan todos los actores del juego
 * @author Juan Carlos Beltran
 */
public class Mundo {
    private SpriteBatch batch; //Batch que dibujará Sprites
    private OrthographicCamera camera; //Cámara para escalar Sprites.
    private World world; //Mundo en el que se dibujarán los actores.
    private Box2DDebugRenderer debugRenderer; //Solo para mostrar formas de depuración.
    private static Texture fondo; //Textura de fondo de pantalla
    private ArrayList<Suelo> elementosFijos; //Suelo del mundo
    private ArrayList<Enemigo> elementosPeligrosos; //Array de los elementos que pueden matar al personaje
    private SpriteBatch batchTexto;
    private BitmapFont textoScore;
    private Puntuaciones partidaActual;

    /**
     * Mundo crea todos sus atributos y llama luego a la funcion recontruir
     */
    public Mundo() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera(40, 30);
        world = new World(new Vector2(0, -98), true);
        debugRenderer = new Box2DDebugRenderer();
        fondo = new Texture(Gdx.files.internal("pantallas/MapaBlackMario2.png"));

        batchTexto=new SpriteBatch();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fuentes/SuperMario256.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 50;
        parameter.borderColor=new Color(1.0f,0.1f,0.1f,1);
        parameter.borderWidth=3f;
        parameter.incremental=true;
        textoScore = generator.generateFont(parameter);
        partidaActual = new Puntuaciones(0, new Timestamp(System.currentTimeMillis()),null);

        camera.position.x = 20;
        camera.position.y = 15;
        camera.update();

        //Construimos el mundo
        reconstruir();

    }

    /**
     * Devuelve el atributo World del mundo
     * @return World el mundo
     */
    public World getWorld() {
        return world;
    }

    /**
     * Devuelve el array de Asesinos de la clase
     * @return ArrayList<Enemigo> array de elementos peligrosos
     */
    public ArrayList<Enemigo> getElementosPeligrosos() {
        return elementosPeligrosos;
    }

    /**
     * Posiciona la camara correctamente
     */
    public void redimensionar() {
        batch.setProjectionMatrix(camera.combined);
    }

    /**
     * Dibuja todos los actores en la pantalla
     * @param personaje Mario que controlara el jugador
     */
    public void dibujar(Mario personaje) {
        // Step the physics world.
        world.step(Gdx.graphics.getDeltaTime(), 8, 4);

        Vector2 velocidad = personaje.getCuerpo().getLinearVelocity();
        if(velocidad.x <= 20 && velocidad.x >= -20) {
            switch (personaje.getMovimiento()) {
                case 'l':
                    personaje.getCuerpo().setLinearVelocity(velocidad.x - 2, velocidad.y);
                    break;
                case 'r':
                    personaje.getCuerpo().setLinearVelocity(velocidad.x + 2, velocidad.y);
                    break;
            }
        }

        //Centro la camara en el personaje cuando no se sale de los bordes
        moverCamara(personaje);

        batch.begin();
        //Dibujo el fondo de este mundo
        batch.draw(fondo, 0, 0, 100, 56);
        //Dibujo todos los actores
        personaje.draw(batch, 0);
        for (int i = 0; i < elementosFijos.size(); i++) {
            elementosFijos.get(i).draw(batch, 0);
        }
        for (int i = 0; i < elementosPeligrosos.size(); i++) {
            elementosPeligrosos.get(i).draw(batch, 0);
        }
        batch.end();

        batchTexto.begin();
        textoScore.draw(batchTexto, "MUERTES : " + partidaActual.getMuertes(),1, Gdx.graphics.getHeight() - 5, Gdx.graphics.getWidth(),1,false);
        batchTexto.end();

        // uncomment to show the physics polygons
       // debugRenderer.render(world, camera.combined);
    }

    /**
     * Crea los actores y los inserta en los array para que se dibujen en pantalla
     */
    public void reconstruir() {
        //Si ya tenemos elementos, los destruimos
        if (elementosFijos != null) {
            for (Suelo elemento : this.elementosFijos) {
                elemento.dispose();
            }
        }

        elementosFijos = new ArrayList<Suelo>();
        for (int distancia = 0; distancia < 300; distancia = distancia + 2) {
            elementosFijos.add(new Suelo(this, distancia, 0, "bloques/suelo1.png"));
            elementosFijos.add(new Suelo(this, -2, distancia, "bloques/suelo1.png"));
            elementosFijos.add(new Suelo(this, 100, distancia, "bloques/suelo1.png"));
        }
        elementosFijos.add(new Suelo(this, 92, 2, "bloques/suelo2.png"));
        elementosPeligrosos = new ArrayList<Enemigo>();
        elementosPeligrosos.add(new Moneda(this, 10, 6));
        elementosPeligrosos.add(new Flor(this, 10, 2));
        elementosPeligrosos.add(new Goomba(this, 14, 2));
        elementosPeligrosos.add(new BloqueTrampa(this, 4, 8));
        elementosPeligrosos.add(new Nube(this, 10, 16));
    }

    /**
     * Hace dispose del mundo y del debug renderer
     */
    public void dispose() {
        world.dispose();
        debugRenderer.dispose();
    }

    /**
     * Funcion que devuelve la camara del mundo
     * @return OrthographicCamera camara del mundo
     */
    public OrthographicCamera getCamera() {
        return camera;
    }

    /**
     * Funcion que devuelve la partida actual
     * @return Puntuaciones de la partida actual
     */
    public Puntuaciones getPartidaActual() {
        return partidaActual;
    }

    /**
     * Funcion que mueve la camara correctamente
     * @param personaje el personaje al que sigue la camara
     */
    public void moverCamara(Mario personaje) {
        if(personaje.getX() >= 20 && personaje.getX() <= 80) {
            camera.position.x = personaje.getX();
            camera.update();
            batch.setProjectionMatrix(camera.combined);
        } else if (personaje.getX() < 20) {
            camera.position.x = 20;
            camera.update();
            batch.setProjectionMatrix(camera.combined);
        }
    }
}
