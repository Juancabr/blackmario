package BaseDeDatosEscritorio;

import java.sql.Timestamp;

import basededatos.BaseDeDatos;
import basededatos.Puntuaciones;

public class BaseDeDatosEscritorio extends BaseDeDatos {
    @Override
    public Puntuaciones cargarPartida() {
        return new Puntuaciones(0,
                new Timestamp(System.currentTimeMillis()),null);
    }

    @Override
    public void guardarPartida(Puntuaciones partida) {
        //No hacer nada
    }

    @Override
    public void finalizarPartida() {
        //no hace nada
    }

    /**
     * Devuelve un top3 ficticio
     * @return
     */
    @Override
    public Puntuaciones[] top3() {
        Puntuaciones[] ret=new Puntuaciones[3];
        ret[0]=new Puntuaciones(5,
                new Timestamp(System.currentTimeMillis()),null);
        ret[1]=new Puntuaciones(4,
                new Timestamp(System.currentTimeMillis()),null);
        ret[2]=new Puntuaciones(3,
                new Timestamp(System.currentTimeMillis()),null);
        return ret;
    }
}
