package elementos;

import com.blackmario.Mundo;

import actores.Mario;

/**
 * Clase que representa una nube que sigue al personaje
 * @author Juan Carlos Beltran
 */
public class Nube extends Enemigo {
    int oldX; // ultima posicion x de la nube

    /**
     * Crea la nube
     * @param w Mundo en el que estara la nube
     * @param posX posicion x de la nube
     * @param posY posicion y de la nube
     */
    public Nube(Mundo w, int posX, float posY) {
        super(w, posX, posY, 4, 2.5f, "personajes/enemigos/bloques/Nube1.png");
        oldX = (int)getX();
    }

    /**
     * Funcion que hace la nube siga al personaje y si este se para vaya hacia el para matarlo
     * @param delta Numero que es mas o menos grande dependiendo de los frames del juego
     */
    public void act(float delta, Mario mario) {
        if((int)getX() == oldX) {
            setY(getY() - 50 * delta);
        } else {
            oldX = (int)getX();
            setY(16);
        }
        if(mario.getX() < getX()) {
            setX(getX() - 20 * delta);
        } else {
            setX(getX() + 20 * delta);
        }
    }
}