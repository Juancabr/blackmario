package elementos;

import com.blackmario.Mundo;

public class Goomba extends Asesino {
    private boolean direccionMovimiento;

    public Goomba(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/Goomba1.png");
        direccionMovimiento = false;
    }

    @Override
    public void act(float delta) {
        if(direccionMovimiento) {
            setX(getX() - 5 * delta);
            if(getX() < 14) {
                direccionMovimiento = false;
            }
        } else {
            setX(getX() + 5 * delta);
            if(getX() > 20) {
                direccionMovimiento = true;
            }
        }
    }
}
