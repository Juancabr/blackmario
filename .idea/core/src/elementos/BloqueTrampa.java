package elementos;

import com.blackmario.Mundo;

public class BloqueTrampa extends Asesino {

    public BloqueTrampa(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/bloques/Bloque3.png");
    }

    @Override
    public void act(float delta) {
        if(getY() > 2.5f){
            setY(getY() - 30 * delta);
        }
    }
}