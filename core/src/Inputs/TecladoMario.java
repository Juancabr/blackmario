package inputs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import actores.Mario;

/** Clase que representa los inputs para controlar el personaje
 * @author Juan Carlos Beltran
 */
public class TecladoMario implements InputProcessor {
    private Mario actor; // Personaje al que se le asignan los input

    /**
     * Crea el atributo actor
     * @param j Mario actor que tendra los inputs de la clase
     */
    public TecladoMario (Mario j){
        this.actor=j;
    }

    /**
     * Funcion que se ejecuta cuando se pulsa un boton
     * @param keycode el boton que se ha pulsado
     * @return si el input esta activado
     */
    @Override
    public boolean keyDown(int keycode) {
        Vector2 velocidad = actor.getCuerpo().getLinearVelocity();
        switch (keycode) {
            case Input.Keys.LEFT:
                actor.setMovimiento('l');
                break;
            case Input.Keys.R:
                actor.getCuerpo().setLinearVelocity(new Vector2(0,0));
                actor.getCuerpo().setAngularVelocity(0);
                actor.getCuerpo().setTransform(actor.getX()+26,2,0);
                break;
            case Input.Keys.RIGHT:
                actor.setMovimiento('r');
                break;
            case Input.Keys.UP:
               if(actor.getY() < 2.5) {
                   actor.getCuerpo().setLinearVelocity(velocidad.x, 40);
               }
                break;
        }
        return true;
    }

    /**
     * Funcion que se ejecuta cuando se deja de pulsar un boton
     * @param keycode el boton que se ha dejado de pulsado
     * @return si el input esta activado
     */
    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.LEFT:
                actor.setMovimiento('a');
                break;
            case Input.Keys.RIGHT:
                actor.setMovimiento('a');
                break;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    /**
     *
     * @param screenX
     * @param screenY
     * @return
     */
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector2 velocidad = actor.getCuerpo().getLinearVelocity();
        int size = Gdx.graphics.getWidth() / 3;
        if(screenX < size) {
            actor.setMovimiento('l');
        } else if (screenX > size * 2){
            actor.setMovimiento('r');
        } else {
            if(actor.getY() < 2.5) {
                actor.getCuerpo().setLinearVelocity(velocidad.x, 40);
            }
        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        int size = Gdx.graphics.getWidth() / 3;
        if(screenX < size) {
            actor.setMovimiento('a');
        } else if (screenX > size * 2){
            actor.setMovimiento('a');
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
