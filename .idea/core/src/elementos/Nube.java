package elementos;

import com.blackmario.Mundo;

import actores.Mario;

public class Nube extends Asesino {
    int oldX;

    public Nube(Mundo w, int posX, float posY) {
        super(w, posX, posY, 4, 2.5f, "personajes/enemigos/bloques/Nube1.png");
        oldX = (int)getX();
    }


    public void act(float delta, Mario mario) {
        if((int)getX() == oldX) {
            setY(getY() - 50 * delta);
        } else {
            oldX = (int)getX();
            setY(16);
        }
        if(mario.getX() < getX()) {
            setX(getX() - 20 * delta);
        } else {
            setX(getX() + 20 * delta);
        }
    }
}