package elementos;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.blackmario.Mundo;

/**
 * Clase que representa cualquier elemento del mundo que mata al personaje
 * @author Juan Carlos Beltran
 */
public abstract class Enemigo extends Actor {
    private Mundo mundo; // El mundo en el que va a estar el enemigo
    private Sprite sprite; // Imagen del enemigo

    /**
     * Crea los atributos del enemigo con los datos que se le pasa por parametro
     * @param w Mundo en el que estara el enemigo
     * @param posX posicion x en la que se creara el enemigo
     * @param posY posicion y en la que se creara el enemigo
     * @param width anchura del enemigo
     * @param height altura del enemigo
     * @param spritePath String ruta de la imagen del enemigo
     */
    public Enemigo(Mundo w, int posX, float posY, float width, float height, String spritePath){
        this.mundo=w;
        //Asignamos la textura
        sprite=new Sprite(new Texture(Gdx.files.internal(spritePath)));
        sprite.setSize(width, height);
        this.setX(posX);
        this.setY(posY);
        sprite.setOrigin((getWidth())/2,(getHeight())/2);
    }

    /**
     * Dibuja el enemigo en el mundo
     */
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        sprite.setScale(getScaleX(),getScaleY());
        sprite.setRotation(getRotation());
        sprite.setPosition(getX(),getY());
        sprite.draw(batch);
    }

    /**
     * Devuelve la hitbox del enemigo
     * @return la hitbox del enemigo
     */
    public Rectangle getHitbox(){
        return this.sprite.getBoundingRectangle();
    }
}
