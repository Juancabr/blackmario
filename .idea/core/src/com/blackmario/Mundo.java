package com.blackmario;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;

import actores.Mario;
import elementos.Asesino;
import elementos.BloqueTrampa;
import elementos.Flor;
import elementos.Goomba;
import elementos.Moneda;
import elementos.Nube;
import elementos.Suelo;

public class Mundo {
    private SpriteBatch batch; //Batch que dibujará Sprites
    private OrthographicCamera camera; //Cámara para escalar Sprites.
    private World world; //Mundo en el que se dibujarán los actores.
    private Box2DDebugRenderer debugRenderer; //Solo para mostrar formas de depuración.
    private static Texture fondo; //Textura de fondo de pantalla
    private ArrayList<Suelo> elementosFijos; //Suelo del mundo
    private ArrayList<Asesino> elementosPeligrosos;

    public Mundo() {

        batch = new SpriteBatch();
        camera = new OrthographicCamera(40, 30);
        world = new World(new Vector2(0, -98), true);
        debugRenderer = new Box2DDebugRenderer();
        fondo = new Texture(Gdx.files.internal("pantallas/MapaBlackMario2.png"));

        camera.position.x = 20;
        camera.position.y = 15;
        camera.update();

        //Construimos el mundo
        reconstruir();

    }

    public World getWorld() {
        return world;
    }

    public ArrayList<Asesino> getElementosPeligrosos() {
        return elementosPeligrosos;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public SpriteBatch getBatch() {
        return batch;
    }

    public void redimensionar() {
        batch.setProjectionMatrix(camera.combined);
    }

    public void dibujar(Mario personaje) {
        // Step the physics world.
        world.step(Gdx.graphics.getDeltaTime(), 8, 4);

        Vector2 velocidad = personaje.getCuerpo().getLinearVelocity();
        if(velocidad.x <= 20 && velocidad.x >= -20) {
            switch (personaje.getMovimiento()) {
                case 'l':
                    personaje.getCuerpo().setLinearVelocity(velocidad.x - 2, velocidad.y);
                    break;
                case 'r':
                    personaje.getCuerpo().setLinearVelocity(velocidad.x + 2, velocidad.y);
                    break;
            }
        }

        //Centro la camara en el personaje cuando no se sale de los bordes
        if(personaje.getX() >= 20 && personaje.getX() <= 80) {
            camera.position.x = personaje.getX();
            camera.update();
            batch.setProjectionMatrix(camera.combined);
        }

        batch.begin();
        //Dibujo el fondo de este mundo
        batch.draw(fondo, 0, 0, 100, 56);
        //Dibujo todos los actores
        personaje.draw(batch, 0);
        for (int i = 0; i < elementosFijos.size(); i++) {
            elementosFijos.get(i).draw(batch, 0);
        }
        for (int i = 0; i < elementosPeligrosos.size(); i++) {
            elementosPeligrosos.get(i).draw(batch, 0);
        }
        batch.end();

        // uncomment to show the physics polygons
       // debugRenderer.render(world, camera.combined);
    }

    public void reconstruir() {
        //Si ya tenemos elementos, los destruimos
        if (elementosFijos != null) {
            for (Suelo elemento : this.elementosFijos) {
                elemento.dispose();
            }
        }

        elementosFijos = new ArrayList<Suelo>();
        for (int distancia = 0; distancia < 300; distancia = distancia + 2) {
            elementosFijos.add(new Suelo(this, distancia, 0, "bloques/suelo1.png"));
            elementosFijos.add(new Suelo(this, -2, distancia, "bloques/suelo1.png"));
            elementosFijos.add(new Suelo(this, 100, distancia, "bloques/suelo1.png"));
        }
        elementosFijos.add(new Suelo(this, 92, 2, "bloques/suelo2.png"));
        elementosPeligrosos = new ArrayList<Asesino>();
        elementosPeligrosos.add(new Moneda(this, 10, 6));
        elementosPeligrosos.add(new Flor(this, 10, 2));
        elementosPeligrosos.add(new Goomba(this, 14, 2));
        elementosPeligrosos.add(new BloqueTrampa(this, 4, 8));
        elementosPeligrosos.add(new Nube(this, 10, 16));
    }

    public void dispose() {
        world.dispose();
        debugRenderer.dispose();
    }
}
