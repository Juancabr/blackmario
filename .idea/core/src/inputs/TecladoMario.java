package inputs;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;

import actores.Mario;

public class TecladoMario implements InputProcessor {
    private Mario actor;

    public TecladoMario (Mario j){
        this.actor=j;
    }

    @Override
    public boolean keyDown(int keycode) {
        Vector2 velocidad = actor.getCuerpo().getLinearVelocity();
        switch (keycode) {
            case Input.Keys.LEFT:
                actor.setMovimiento('l');
                break;
            case Input.Keys.R:
                actor.getCuerpo().setLinearVelocity(new Vector2(0,0));
                actor.getCuerpo().setAngularVelocity(0);
                actor.getCuerpo().setTransform(actor.getX()+26,2,0);
                break;
            case Input.Keys.RIGHT:
                actor.setMovimiento('r');
                break;
            case Input.Keys.UP:
                actor.getCuerpo().setLinearVelocity(velocidad.x, 40);
                break;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.LEFT:
                actor.setMovimiento('a');
                break;
            case Input.Keys.RIGHT:
                actor.setMovimiento('a');
                break;
        }
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
