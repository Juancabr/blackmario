package basededatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseDeDatosAndroidOpenHelper extends SQLiteOpenHelper {

    public BaseDeDatosAndroidOpenHelper(Context c){
        super(c,BaseDeDatos.getDatabaseName(),null,BaseDeDatos.getDatabaseVersion());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BaseDeDatos.getDatabaseCreationQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(BaseDeDatos.getDatabaseUpdateQuery());
    }
}
