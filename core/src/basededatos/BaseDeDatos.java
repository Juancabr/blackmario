package basededatos;

public abstract class BaseDeDatos {
    private static int databaseVersion;
    private static String databaseName;
    private static String SCORE_TABLENAME;
    private static String SCORE_FIELD;
    private static String STARTDATE_FIELD;
    private static String ENDDATE_FIELD;
    private static String databaseCreationQuery;
    private static String databaseUpdateQuery;

    public abstract Puntuaciones cargarPartida();

    public abstract void guardarPartida(Puntuaciones partida);

    public abstract void finalizarPartida();

    public abstract Puntuaciones[] top3();

    public BaseDeDatos() {
        databaseVersion = 1;
        databaseName = "blackmario";
        SCORE_TABLENAME = "puntuaciones";
        SCORE_FIELD = "muertes";
        STARTDATE_FIELD = "fInicio";
        ENDDATE_FIELD = "fFinal";
        databaseCreationQuery="CREATE TABLE "+SCORE_TABLENAME+" (" +
                STARTDATE_FIELD +" TIMESTAMP PRIMARY KEY NOT NULL DEFAULT CURRENT_TIMESTAMP," +
                SCORE_FIELD + " INT DEFAULT 0 ," +
                ENDDATE_FIELD +" TIMESTAMP DEFAULT NULL" +
                ");";
        databaseUpdateQuery="";
    }

    public static int getDatabaseVersion() {
        return databaseVersion;
    }

    public static String getDatabaseName() {
        return databaseName;
    }

    public static String getScoreTablename() {
        return SCORE_TABLENAME;
    }

    public static String getScoreField() {
        return SCORE_FIELD;
    }

    public static String getStartdateField() {
        return STARTDATE_FIELD;
    }

    public static String getEnddateField() {
        return ENDDATE_FIELD;
    }

    public static String getDatabaseCreationQuery() {
        return databaseCreationQuery;
    }

    public static String getDatabaseUpdateQuery() {
        return databaseUpdateQuery;
    }
}
