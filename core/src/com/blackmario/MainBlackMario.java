package com.blackmario;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;

import actores.Mario;
import elementos.Nube;
import inputs.TecladoMario;

/** Clase que crea el mundo, el personaje y el input
 * @author Juan Carlos Beltran
 */

public class MainBlackMario extends ApplicationAdapter {
    private Mario personaje;
	private Mundo mundo;
    private TecladoMario teclado;

    /**
     * Crea el mundo, personaje e input del personaje
     */
	@Override
	public void create () {
        mundo = new Mundo();
        personaje = new Mario(mundo);
        teclado = new TecladoMario(personaje);
        Gdx.input.setInputProcessor(teclado);
	}

    /**
     * Se ejecuta cada frame del juego. Llama a las funciones de dibujo del mundo y a la funcion de gestion de colisiones
     */
	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mundo.dibujar(this.personaje);

        //movimientos de los enemigos
        mundo.getElementosPeligrosos().get(2).act(Gdx.graphics.getDeltaTime());

        if(personaje.getX() > mundo.getElementosPeligrosos().get(3).getX()-2.5f) {
            mundo.getElementosPeligrosos().get(3).act(Gdx.graphics.getDeltaTime());
        }

        if(personaje.getX() > 16) {
            Nube nube = (Nube) mundo.getElementosPeligrosos().get(4);
            nube.act(Gdx.graphics.getDeltaTime(), personaje);
        }

        gestionColisiones();
	}

    /**
     * Se ejecuta cuando se redimensiona la pantalla y llama a la funcion de redimension del mundo
     */
    @Override
    public void resize(int width, int height) {
        mundo.redimensionar();
    }

    /**
     * Hace dispose del mundo
     */
	@Override
	public void dispose () {
        mundo.dispose();
	}

    /**
     * Funcion que detecta si hay una colision en alguno de los actores que hay en el mundo
     */
    public void gestionColisiones(){
        for (int i = 0; i < mundo.getElementosPeligrosos().size(); i++) {
            if(personaje.colisionaCon(mundo.getElementosPeligrosos().get(i))) {
                mundo.getCamera().position.x = 20;
                mundo.getCamera().position.y = 15;
                mundo.getCamera().update();

                this.mundo.reconstruir();

                personaje.getCuerpo().setLinearVelocity(new Vector2(0,0));
                personaje.getCuerpo().setAngularVelocity(0);
                personaje.getCuerpo().setTransform(0,4,0);
                mundo.getPartidaActual().setMuertes(mundo.getPartidaActual().getMuertes() + 1);
            }
        }

        //Si llega a la meta
        if(personaje.getCuerpo().getPosition().x > 92){
            this.mundo.reconstruir();

            personaje.getCuerpo().setLinearVelocity(new Vector2(0,0));
            personaje.getCuerpo().setAngularVelocity(0);
            personaje.getCuerpo().setTransform(0,2,0);

            mundo.getPartidaActual().setMuertes(0);
        }
    }
}
