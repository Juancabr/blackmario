package elementos;

import com.blackmario.Mundo;

/**
 * Clase que representa un monstruo que se mueve de un lado a otro
 * @author Juan Carlos Beltran
 */
public class Goomba extends Enemigo {
    private boolean direccionMovimiento; // direccion del enemigo

    /**
     * Crea el enemigo
     * @param w Mundo en el que estara el enemigo
     * @param posX posicion x de el enemigo
     * @param posY posicion y de el enemigo
     */
    public Goomba(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/Goomba1.png");
        direccionMovimiento = false;
    }

    /**
     * Funcion que hace que el goomba se mueve de un lado a otro
     * @param delta Numero que es mas o menos grande dependiendo de los frames del juego
     */
    @Override
    public void act(float delta) {
        if(direccionMovimiento) {
            setX(getX() - 5 * delta);
            if(getX() < 14) {
                direccionMovimiento = false;
            }
        } else {
            setX(getX() + 5 * delta);
            if(getX() > 20) {
                direccionMovimiento = true;
            }
        }
    }
}
