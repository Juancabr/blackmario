package com.blackmario;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

public class Inicio extends Activity {

    @Override
    protected void onStart() {
        super.onStart();
        setContentView(R.layout.activity_inicio);
        Intent servicio=new Intent(this, Servicio.class);
        startService(servicio);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Estas seguro de que quieres salir?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Inicio.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClick1(View view) {
        Intent i = new Intent(this, AndroidLauncher.class);
        startActivity(i);
    }
}
