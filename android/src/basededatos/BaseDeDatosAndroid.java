package basededatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.badlogic.gdx.Gdx;

import java.sql.Timestamp;

public class BaseDeDatosAndroid extends BaseDeDatos {
    private BaseDeDatosAndroidOpenHelper openHelper;
    private SQLiteDatabase baseDatos;

    /**
     * Obtener puntuación de la partida actual , si no está finalizada
     * Es decir: voy a buscar en base de datos una partida que tenga
     * la hora de finalización a null, y esa será mi partida actual.
     * @return
     */
    public BaseDeDatosAndroid(Context c){
        super();
        openHelper=new BaseDeDatosAndroidOpenHelper(c);
        baseDatos=openHelper.getWritableDatabase();
    }

    /**
     * Esta función devuelve la partida actual, o una partida nueva si no hay ninguna en base de datos
     * @return partida en curso, ya sea una ya empezada, o una nueva
     */
    @Override
    public Puntuaciones cargarPartida() {
        //Hago la consulta a ver si hay una partida en curso (Con fecha de fin igual a null)
        Cursor c=baseDatos.query(BaseDeDatos.getScoreTablename(),null,
                BaseDeDatos.getEnddateField()+" is null",null,
                null,null,null);
        //Si existe una partida guardada ya
        if(c.getCount()>0){
            //Cargo la partida en un objeto Puntuaciones y lo devuelvo
            c.moveToFirst();
            int nBlobsComidos= c.getInt(c.getColumnIndex(BaseDeDatos.getScoreField()));
            Timestamp fInicio=new Timestamp(c.getLong(c.getColumnIndex(BaseDeDatos.getStartdateField())));
            Timestamp fFin=new Timestamp(c.getLong(c.getColumnIndex(BaseDeDatos.getEnddateField())));
            Puntuaciones ret=new Puntuaciones(nBlobsComidos,fInicio,fFin);
            return ret;
        }else{ //Si no se ha encontrado una partida empezada en la base de datos
            //Creo una nueva partida que comienza ahora
            return new Puntuaciones(0,new Timestamp(System.currentTimeMillis()),null);
        }

    }

    /** va a meter en base de datos la puntuación actual y la fecha de inicio
     * pero va a dejar la fecha de fin a null, para marcarla como partida
     * actual
     * @param partida la partida que voy a guardar
     */
    @Override
    public void guardarPartida(Puntuaciones partida) {
        //Primero compruebo si hay una partida abierta para insertarla o actualizar la que hay
        Cursor c=baseDatos.query(BaseDeDatos.getScoreTablename(),null,
                BaseDeDatos.getEnddateField()+" is null",null,
                null,null,null);

        //Si la partida con fecha final = null existe ya, la modifico
        if(c.getCount()>0) { //Modificar la partida ya abierta
            c.moveToFirst();
            ContentValues valores=new ContentValues();
            valores.put(BaseDeDatos.getScoreField(),partida.getMuertes());
            baseDatos.update(BaseDeDatos.getScoreTablename(),valores,
                    BaseDeDatos.getEnddateField()+" is null",null);

            //Si la partida con fecha final = null aún no existe, la inserto
        }else{ //insertar una nueva partida
            Gdx.app.log("Voy a insertar","!!!!!!!" );
            ContentValues valores=new ContentValues();
            valores.put(BaseDeDatos.getScoreField(),partida.getMuertes());
            valores.put(BaseDeDatos.getStartdateField(),partida.getInicioPartida().getTime());
            valores.put(BaseDeDatos.getEnddateField(),"null");
            Long id=baseDatos.insert(BaseDeDatos.getScoreTablename(),null,valores);
            Gdx.app.log("ID insertada",id+"" );
        }
    }

    /**
     * Ponerle fecha de final a la partida actual, dándola por finalizada
     * Prerrequsito: Por narices se ha tenido que guardar la partida
     * antes de llamar a esta función
     */
    @Override
    public void finalizarPartida() {
        ContentValues valores=new ContentValues();
        valores.put(BaseDeDatos.getEnddateField(),System.currentTimeMillis());
        baseDatos.update(BaseDeDatos.getScoreTablename(),valores,
                BaseDeDatos.getEnddateField()+" is null",null);
    }

    /**
     * las tres partidas finalizadas con mayor puntuación
     * @return
     */
    @Override
    public Puntuaciones[] top3() {
        return new Puntuaciones[0];
    }
}
