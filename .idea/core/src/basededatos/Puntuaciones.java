package basededatos;

import java.sql.Timestamp;

public class Puntuaciones {
    private Timestamp inicioPartida;
    private Timestamp finPartida;

    public Puntuaciones(Timestamp ip,Timestamp fp){
        this.inicioPartida=ip;
        this.finPartida=fp;
    }

    public Timestamp getInicioPartida() {
        return inicioPartida;
    }

    public void setInicioPartida(Timestamp inicioPartida) {
        this.inicioPartida = inicioPartida;
    }

    public Timestamp getFinPartida() {
        return finPartida;
    }

    public void setFinPartida(Timestamp finPartida) {
        this.finPartida = finPartida;
    }


}
