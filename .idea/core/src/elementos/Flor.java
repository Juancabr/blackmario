package elementos;

import com.blackmario.Mundo;

public class Flor extends Asesino {

    public Flor(Mundo w, int posX, float posY) {
        super(w, posX, posY, 2, 2, "personajes/enemigos/items/Flor1.png");
    }
}
